﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hook manager.
/// </summary>
public class HookManager : MonoBehaviour
{
	/// <summary>
	/// Types of rotation.
	/// </summary>
	enum Rotation
	{
		VERTICAL_CHAIN,
		HORIZONTAL_CHAIN
	}

	/// <summary>
	/// Hook action.
	/// </summary>
	enum HookAction
	{
		FORWARD,
		BACKWARD,
		STOPPED
	}

	/// <summary>
	/// A piece of chain prefab.
	/// </summary>
	public GameObject chainPrefab;
	/// <summary>
	/// The hook spawn point.
	/// </summary>
	public GameObject spawnPoint;
	/// <summary>
	/// The forward collider.
	/// </summary>
	public Collider forwardCollider;
	/// <summary>
	/// The backward collider.
	/// </summary>
	public Collider backwardCollider;
	/// <summary>
	/// The hook speed.
	/// </summary>
	public float hookSpeed = 5;
	/// <summary>
	/// The last chain instance identifier. Required to avoid double chain instantiation
	/// </summary>
	private int lastChainInstanceId;
	/// <summary>
	/// The actual hook speed.
	/// </summary>
	private float actualHookSpeed;
	/// <summary>
	/// The current piece of chain rotation.
	/// </summary>
	private Rotation chainRotation;
	/// <summary>
	/// The horizontal rotation.
	/// </summary>
	private Vector3 horizontalRotation = new Vector3 (0, 270, 90);
	/// <summary>
	/// The vertical rotation.
	/// </summary>
	private Vector3 verticalRotation = Vector3.zero;
	/// <summary>
	/// The hook action.
	/// </summary>
	private HookAction hookAction;
	/// <summary>
	/// The instantiated chains stack.
	/// </summary>
	private Stack<GameObject> chainStack;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake ()
	{
		ResetLastChainInstanceId ();
		chainRotation = Rotation.VERTICAL_CHAIN;
		SetHookAction (HookAction.STOPPED);
		chainStack = new Stack<GameObject> ();
		chainStack.Push (gameObject);
		// Debug only
		Time.timeScale = .7f;
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		CheckForInput ();
		UpdateHookMovement ();
		MoveHook ();
	}

	/// <summary>
	/// Checks for external input.
	/// </summary>
	private void CheckForInput ()
	{
		if (Input.GetKey (KeyCode.UpArrow)) {
			SetHookAction (HookAction.FORWARD);
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			SetHookAction (HookAction.BACKWARD);
		} else if (Input.GetKey (KeyCode.Space)) {
			SetHookAction (HookAction.STOPPED);
		}
	}

	/// <summary>
	/// Updates the hook movement.
	/// </summary>
	private void UpdateHookMovement ()
	{
		switch (hookAction) {
		case HookAction.FORWARD:
			actualHookSpeed = hookSpeed;
			break;
		case HookAction.BACKWARD:
			actualHookSpeed = -hookSpeed;
			break;
		case HookAction.STOPPED:
			actualHookSpeed = 0;
			break;
		default:
			Debug.LogWarning ("UpdateHookMovement - L'azione del gancio '" + chainRotation + "' non è gestita!!!");
			actualHookSpeed = 0;
			break;
		}
	}

	/// <summary>
	/// Moves the hook.
	/// </summary>
	private void MoveHook ()
	{
		transform.Translate (Vector3.forward * actualHookSpeed * Time.deltaTime);
	}

	/// <summary>
	/// Updates the piece of chain rotation.
	/// </summary>
	private void UpdateChainRotation ()
	{
		switch (chainRotation) {
		case Rotation.HORIZONTAL_CHAIN:
			chainRotation = Rotation.VERTICAL_CHAIN;
			break;
		case Rotation.VERTICAL_CHAIN:
			chainRotation = Rotation.HORIZONTAL_CHAIN;
			break;
		default:
			Debug.LogWarning ("UpdateChainRotation - La rotazione della catena '" + chainRotation + "' non è gestita!!!");
			break;
		}
	}

	/// <summary>
	/// Adds a piece of chain.
	/// </summary>
	public void AddChain ()
	{
		GameObject nextChain;
		FixedJoint joint;

		nextChain = Instantiate (chainPrefab);
		nextChain.transform.position = spawnPoint.transform.position;
		nextChain.transform.rotation = Quaternion.FromToRotation (nextChain.transform.forward, spawnPoint.transform.forward);
		//RotateChain (nextChain);
		joint = nextChain.GetComponent<FixedJoint> ();
		Debug.Log ("AddChain - joint: " + joint + " --- Cima stack: " + chainStack.Peek ());
		joint.connectedBody = chainStack.Peek ().GetComponent<Rigidbody> ();
		chainStack.Push (nextChain);
		UpdateChainRotation ();
	}

	/// <summary>
	/// Removes a piece of chain.
	/// </summary>
	/// <param name="chain">Piece of chain to remove.</param>
	public void RemoveChain (GameObject chain)
	{
		//Debug.Log ("RemoveChain - ID cima stack: " + chainStack.peek().GetInstanceID ());
		//Debug.Log ("RemoveChain - ID da distruggere: " + chain.GetInstanceID ());
		Destroy (chainStack.Pop ());
	}

	/// <summary>
	/// Rotates a piece of chain.
	/// </summary>
	/// <param name="chain">Piece of chain to rotate.</param>
	private void RotateChain (GameObject chain)
	{
		switch (chainRotation) {
		case Rotation.HORIZONTAL_CHAIN:
			chain.transform.Rotate (horizontalRotation);
			break;
		case Rotation.VERTICAL_CHAIN:
			chain.transform.Rotate (horizontalRotation);
			break;
		default:
			Debug.LogWarning ("RotateChain - La rotazione della catena '" + chainRotation + "' non è gestita!!!");
			break;
		}
	}

	/// <summary>
	/// Determines whether the hook movement is forward.
	/// </summary>
	/// <returns><c>true</c> if the hook movement is forward; otherwise, <c>false</c>.</returns>
	private bool IsForwardMovement ()
	{
		return hookAction == HookAction.FORWARD;
	}

	/// <summary>
	/// Determines whether the hook movement is backward.
	/// </summary>
	/// <returns><c>true</c> if the hook movement is backward; otherwise, <c>false</c>.</returns>
	private bool IsBackwardMovement ()
	{
		return hookAction == HookAction.BACKWARD;
	}

	/// <summary>
	/// Resets the last chain instance identifier.
	/// </summary>
	public void ResetLastChainInstanceId ()
	{
		lastChainInstanceId = spawnPoint.GetInstanceID ();
	}

	private void SetHookAction (HookAction hookAction)
	{
		this.hookAction = hookAction;
		switch (hookAction) {
		case HookAction.FORWARD:
			forwardCollider.enabled = true;
			backwardCollider.enabled = false;
			break;
		case HookAction.BACKWARD:
			forwardCollider.enabled = false;
			backwardCollider.enabled = true;
			break;
		case HookAction.STOPPED:
			forwardCollider.enabled = false;
			backwardCollider.enabled = false;
			break;
		default:
			Debug.LogWarning ("SetHookAction - L'azione del gancio '" + chainRotation + "' non è gestita!!!");
			break;
		}
		Debug.Log ("SetHookAction - Forward: " + forwardCollider.enabled + " --- Backward: " + backwardCollider.enabled);
	}

	/// <summary>
	/// Determines whether a chain is last chain element.
	/// </summary>
	/// <returns><c>true</c> if this instance is last chain element; otherwise, <c>false</c>.</returns>
	/// <param name="chain">Chain.</param>
	public bool IsLastChainElement (GameObject chain)
	{
		return chainStack.Peek () == chain;
	}

	/// <summary>
	/// Check if a china has the already generated a new chain element.
	/// </summary>
	/// <returns><c>true</c>, if already generated, <c>false</c> otherwise.</returns>
	/// <param name="chain">Chain.</param>
	public bool HasAlreadyGenerated (GameObject chain)
	{
		return lastChainInstanceId == chain.GetInstanceID ();
	}

	/// <summary>
	/// Updates the last chain element.
	/// </summary>
	/// <param name="chain">New last chain elemen.</param>
	public void UpdateLastChainElement (GameObject chain)
	{
		lastChainInstanceId = chain.GetInstanceID ();
	}

	/// <summary>
	/// Manages a hook collision.
	/// </summary>
	/// <param name="collision">Collision.</param>
	void OnCollisionEnter (Collision collision)
	{
		Debug.Log ("OnCollisionEnter - L'hook ha colliso con " + collision.gameObject.tag);
		SetHookAction (HookAction.STOPPED);
	}
}
