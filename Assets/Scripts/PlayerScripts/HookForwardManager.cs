﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hook forward manager.
/// </summary>
public class HookForwardManager : MonoBehaviour
{
	/// <summary>
	/// The hook manager. Don't use singleton, otherwise all players hooks react to the trigger
	/// </summary>
	public HookManager hookManager;

	/// <summary>
	/// Remove a piece of chain.
	/// </summary>
	/// <param name="other">Piece of chain trigged.</param>
	void OnTriggerEnter (Collider other)
	{
		Debug.Log ("OnTriggerEnter - Sollevato evento forward >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>O");
		if (!hookManager.HasAlreadyGenerated (other.gameObject) && hookManager.IsLastChainElement (other.gameObject)) {
			Debug.Log ("OnTriggerEnter - Tag dell'oggetto colliso: " + other.gameObject.tag + " (" + other.gameObject.GetInstanceID () + ")");
			if (other.tag.Equals ("Chain") || other.tag.Equals ("Hook")) {
				hookManager.UpdateLastChainElement (other.gameObject);
				hookManager.AddChain ();
			}
		}
	}
}
