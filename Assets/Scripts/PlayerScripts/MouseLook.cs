﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MouseLook : NetworkBehaviour {

	float aimSpeed = 10;
//	float maxAimSpeed = 20;

	// Rotation stuff
	public float minYRotation = -85.0f;
	public float maxYRotation = 85.0f; 
	float currentYRotation = 0.0f;
	Quaternion yRotation;


//	void Update () {
//		MouseAiming ();	
//	}
	public void MouseAiming () {

		float mouseHorizontal = Input.GetAxis ("Mouse X") * aimSpeed;
		float mouseVertical = - Input.GetAxis ("Mouse Y") * aimSpeed;

		// rotate Horizontally body+face
		transform.parent.localEulerAngles += new Vector3 (0, mouseHorizontal, 0);

		// Clamp and rotate face horizontally
		currentYRotation += mouseVertical;
		currentYRotation = Mathf.Clamp(currentYRotation, minYRotation, maxYRotation);
		transform.localRotation = Quaternion.Euler (new Vector3 (currentYRotation, 0, 0)) ;

	}
}
