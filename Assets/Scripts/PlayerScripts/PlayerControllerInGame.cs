﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerControllerInGame : NetworkBehaviour {

    [HideInInspector]
    public bool isOffline = false;
    //[HideInInspector]
    public Rigidbody myRb;
	float speed = 20;
	float clampMaxSpeed = 12;
    float jumpForce = 5;
    [SyncVar] int myScore = 0;
    float timer;
	public Transform faceTransform;
    //[HideInInspector]
	public MouseLook mouseLook;
    public float distToGround;
    public enum PlayerStates {
        ground,
        inMovement,
        jumping,
        hooking,
        notHooking
    }

    public PlayerStates playerStatus;
    float bulletSpeed = 20;

    Hook hook;
    public override void OnStartLocalPlayer()
	{
        distToGround = GetComponent<Collider>().bounds.extents.y;
		myRb = GetComponent <Rigidbody> ();
		mouseLook = GetComponentInChildren <MouseLook> ();
		Camera.main.GetComponent <CameraTargetPosition>().cameraPosTarget = transform.FindChild ("Face");
        playerStatus = PlayerStates.ground;
        hook = GetComponent<Hook>();
		GameManager.instance.AddPlayerToList (this.gameObject);
	}

    // Update is called once per frame
    void FixedUpdate() {
        if (!isOffline && !isLocalPlayer) {
            return;
        }

        switch (playerStatus) {
            case PlayerStates.ground:
                if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0) {
                    Move(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
                }
                if (Input.GetKeyDown(KeyCode.Space)) {
                    myRb.velocity = new Vector3(myRb.velocity.x, myRb.velocity.y + jumpForce, myRb.velocity.z);
                    playerStatus = PlayerStates.jumping;
                }
                break;
            case PlayerStates.jumping:
                if (Input.GetAxis("Horizontal") != 0) {
                    JumpMove(Input.GetAxis("Horizontal") / 10);
                }
                break;
            case PlayerStates.hooking:
                break;
            case PlayerStates.notHooking:
                break;

            default:
                Debug.Log("Where the fuck am I?");
                break;

        }
        if (myRb.velocity.y <= 0) {
            playerStatus = OnGround();
        }
    }

	void Update () {
		if (!isOffline && !isLocalPlayer) {
			return;
		}
		mouseLook.MouseAiming ();
	}

	void Move (float vertAxis, float horizAxis) {
			Vector3 movingVelocity = new Vector3 (horizAxis * speed, myRb.velocity.y, vertAxis * speed);
			Vector3 directionVel = transform.TransformDirection (movingVelocity); 
			myRb.velocity = new Vector3 (directionVel.x, myRb.velocity.y, directionVel.z);
			myRb.velocity = Vector3.ClampMagnitude (myRb.velocity, clampMaxSpeed);
            myRb.angularVelocity = Vector3.zero;
    }
    void JumpMove(float horizAxis) {
        myRb.velocity += transform.TransformDirection(new Vector3(horizAxis,0,0));
    }

    PlayerStates OnGround() {
        PlayerStates ps;
        if (Physics.Raycast(transform.position, Vector3.down,distToGround+0.1f,~LayerMask.NameToLayer("Player"))) {
            ps = PlayerStates.ground;
        } else {
            ps = PlayerStates.jumping;
        }
        return ps;
    }
    private void OnCollisionEnter(Collision collision) {
        myRb.isKinematic = false;
        hook.hs = Hook.HookStatus.notHooking;
    }

    public void AddScore(int amount, float givePoints) {
        timer += Time.deltaTime;
        if(timer > givePoints)
        {
            myScore += amount;
            timer = 0;
        }
        
    }
}
