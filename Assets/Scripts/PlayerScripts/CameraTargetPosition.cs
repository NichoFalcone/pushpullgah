﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CameraTargetPosition : MonoBehaviour {

	public Transform cameraPosTarget;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        /*if (!isLocalPlayer)
        {
            return;
        }*/
        if (cameraPosTarget != null) {
			transform.position = cameraPosTarget.position;
			transform.rotation = cameraPosTarget.rotation;
		}
	}
}
