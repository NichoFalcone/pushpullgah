﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PullHook : NetworkBehaviour
{

    //public float sling;
    public float speed;
    private float step;

    enum HookStatus
    {
        hooking,
        notHooking,
        def
    }

    HookStatus hs;


    public Hook hook;

    public PlayerControllerInGame playerController;

    public LayerMask playerMask;

    private RaycastHit hitInfo;

    public Vector3? slingVector = null;

    public Transform camera;

    public Rigidbody enemyRb;
    //public Transform face;

    public override void OnStartLocalPlayer()
    {
    //    base.OnStartLocalPlayer();
        //face = Camera.main.transform;
    //    step = speed * Time.deltaTime;
    //    hs = HookStatus.def;
    }

    private void Start()
    {
        name = Random.Range(0, 100).ToString();
        //face = Camera.main.transform;
        playerController = hook.playerController;
        camera = hook.firstPersonCamera;
        step = speed * Time.deltaTime;
        hs = HookStatus.def;
    }

    private void Update()
    {
        if(!playerController.isOffline && !isLocalPlayer)
        {
            return;
        }

        Debug.Log(name + " Passing");
        if (Input.GetButtonDown("Fire2"))
        {
            PullEnemy();
        }
        if (Input.GetButtonUp("Fire2"))
        {
            hs = HookStatus.def;
        }

        switch (hs)
        {
            case HookStatus.hooking:
                OnStartHooking(hitInfo.transform, transform);
                break;
            case HookStatus.notHooking:
                //OnStopHooking();
                break;
            case HookStatus.def:
                break;
        }
    }

    void PullEnemy()
    {
        if (Physics.Raycast(camera.position, camera.forward, out hitInfo))
        {
            //GameObject hitGO = hitInfo.transform.gameObject;
            if (hitInfo.transform.gameObject.layer == 20)
            {
                //Debug.Log("SSSSSS");
                hs = HookStatus.hooking;
                enemyRb = hitInfo.transform.gameObject.GetComponent<Rigidbody>();
                if (enemyRb != null)
                enemyRb.isKinematic = true;
            }
        }
    }

    void OnStartHooking(Transform target, Transform endPos)
    {
        //if (!playerController.isCollidingWithOtherPlayer)
        //{
        //    target.position = Vector3.MoveTowards(target.position, endPos.position, step);
        //}
        if(slingVector == null)
        {
            //Vector3 moveVector0 = Vector3.MoveTowards(target.position, endPos.position, step);
            slingVector = endPos.position - target.position;
            //Debug.Log("moveVector = " + moveVector);
            //Debug.Log("moveVector2 = " + moveVector2);
            //slingVector = moveVector;
            //Debug.Log("Salvo, perché ci hai dato un progetto online... adesso mi impicco");
            enemyRb.velocity = (Vector3)slingVector;
        }
        if (slingVector != null)
        {
            //Debug.Log(slingVector);
            //enemyRb.velocity = (Vector3)slingVector;
            slingVector = null;
        }
        if (enemyRb != null)
            enemyRb.isKinematic = false;
        hs = HookStatus.def;
    }

    void OnStopHooking()
    {
        if (slingVector != null)
        {
            //Debug.Log(slingVector);
            //enemyRb.velocity = (Vector3)slingVector;
            slingVector = null;
        }
        if (enemyRb != null)
            enemyRb.isKinematic = false;
        hs = HookStatus.def;
    }
}
