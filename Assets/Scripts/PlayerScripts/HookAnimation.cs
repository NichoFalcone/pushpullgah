﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookAnimation : MonoBehaviour {
	public Transform endPoint;
	public Transform startPoint;
	public Transform link;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float linkSize;

		linkSize = Vector3.Distance (startPoint.position, endPoint.position);
		link.localScale = new Vector3 (0.1f, linkSize, 0.1f);
	}
}
