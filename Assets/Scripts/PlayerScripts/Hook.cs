﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Hook : NetworkBehaviour {

    public Transform firstPersonCamera;
    public float speed;
    public Rigidbody playerRigidbody;
    public PlayerControllerInGame playerController;
    public GameObject hook;
    public Transform spawnHook;
    public Vector3 hookedPosition;

    private float momentum;
    private RaycastHit hit;
    private float step = 20;
    private GameObject playerHook;
    private float maxHookDistance = 10f;



    public enum HookStatus {
        notHooking,
        hooking,
        hooked
    }
    public HookStatus hs;


    //public RigidbodyFirstPersonController fpsController;
    // Use this for initialization
    public override void OnStartLocalPlayer() {
        Debug.Log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> OnStartLocalPlayer");
        playerController = GetComponent<PlayerControllerInGame>();
        firstPersonCamera = Camera.main.transform;
        playerRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        if (!playerController.isOffline && !isLocalPlayer) {
            return;
        }

        if (Input.GetButtonDown("Fire1")) {
            if (Cursor.visible) {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            if (Physics.Raycast(firstPersonCamera.transform.position, firstPersonCamera.forward, out hit)) {
                hs = HookStatus.hooking;
                playerRigidbody.isKinematic = true;
                playerController.playerStatus = PlayerControllerInGame.PlayerStates.hooking;
            }
        }
        if (Input.GetButtonDown("Fire2")) {
            if (Physics.Raycast(firstPersonCamera.transform.position, firstPersonCamera.forward, out hit)) {
                CheckHookOtherPlayer();
            }

        }

        if(hs == HookStatus.hooked) {

        }
        switch (hs) {
            case HookStatus.hooking:
                OnStartHooking(transform, hit.point);
                break;
            case HookStatus.notHooking:
                OnStopHooking();
                break;
            case HookStatus.hooked:
                OnStartHooking(transform, hookedPosition);
                break;
            default:
                Debug.Log("What the fuck am I hooking for ?");
                break;
        }
    }

    void OnStartHooking(Transform T, Vector3 pos) {
        if (Vector3.Distance(T.position, pos) > 2) {
            transform.position = Vector3.MoveTowards(T.position, pos, Time.deltaTime * step);
        } else {
            playerRigidbody.isKinematic = false;
            hs = HookStatus.notHooking;
        }

    }

    void OnStopHooking() {

    }

    void CheckHookOtherPlayer() {
        if (hit.transform.CompareTag("Player")) {
            Hook h = hit.transform.GetComponent<Hook>();
            h.hs = HookStatus.hooked;
            h.hookedPosition = transform.position;

        }
    }
}
