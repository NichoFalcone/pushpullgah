﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hook backward manager.
/// </summary>
public class HookBackwardManager : MonoBehaviour
{
	/// <summary>
	/// The hook manager. Don't use singleton, otherwise all players hooks react to the trigger
	/// </summary>
	public HookManager hookManager;

	/// <summary>
	/// Remove a piece of chain.
	/// </summary>
	/// <param name="other">Piece of chain trigged.</param>
	void OnTriggerEnter (Collider other)
	{
		Debug.Log ("OnTriggerEnter - Sollevato evento backward <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<O");
		hookManager.ResetLastChainInstanceId ();
		// TODO valutare se l'oggetto hook deve essere rimosso o meno
		if (other.tag.Equals ("Chain")) {
			hookManager.RemoveChain (other.gameObject);
		}
	}
}
