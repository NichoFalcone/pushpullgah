﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ControlPoint: NetworkBehaviour {

	private Renderer rend;
	[SyncVar]public bool isActive = false;
	public Color activeColor;
	public Color passiveColor;
    private int scoreToAdd;
    private float scoreTimer;



    private void Awake()
	{
		rend = GetComponent<Renderer>();
        int scoreToAdd = GameManager.instance.scoreAdded;
        float scoreTimer = GameManager.instance.addScoreTimer;
    }

	void Update ()
	{
		rend.material.SetColor("_Color",isActive? activeColor:passiveColor);
        if (isActive)
        {
            rend.material.EnableKeyword("_EMISSION");
        }
        else
        {
            rend.material.DisableKeyword("_EMISSION");
        }
            
	}

	private void OnTriggerEnter(Collider other)
	{
		
	}

	private void OnTriggerStay(Collider other){
        if(isActive) {
            PlayerControllerInGame PlayerOnControl = other.GetComponent<PlayerControllerInGame>();
            PlayerOnControl.AddScore(scoreToAdd, scoreTimer);
        }
    }

	private void OnTriggerExit(Collider other)
	{
		
	}


}