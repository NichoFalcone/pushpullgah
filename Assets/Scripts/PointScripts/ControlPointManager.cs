﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ControlPointManager : NetworkBehaviour {

	public List<ControlPoint> controlPoints;
	[SyncVar(hook = "OnChangeActiveControlPoint")]
	private int controlPointIndex;
	private float timer;
	public float changeZoneTimer;
	
	void Start ()
    {
		if (isServer){
			controlPointIndex = Random.Range (0, controlPoints.Count - 1);
			controlPoints [controlPointIndex].isActive = true;
		}
	}

	void Update ()
    {
		int nextActivePointIndex;


		if (isServer) {
			timer += Time.deltaTime;
			if (timer > changeZoneTimer) {
				do {
					nextActivePointIndex = Random.Range (0, controlPoints.Count - 1);
				} while(nextActivePointIndex == controlPointIndex);
				controlPointIndex = nextActivePointIndex;

			}
		}
	}
		
	private void OnChangeActiveControlPoint(int activePointIndex){
		for (int controlPoint = 0; controlPoint < controlPoints.Count - 1; controlPoint++) {
			controlPoints [controlPoint].isActive = (controlPoint == activePointIndex);
		}
		timer = 0;
	}



}
