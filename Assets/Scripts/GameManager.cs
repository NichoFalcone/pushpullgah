﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {
    //[HideInInspector]
	public List<NetworkInstanceId> playerOnControlPoint = new List<NetworkInstanceId>();
    [HideInInspector]
    public List<GameObject> playerList = new List<GameObject>(); 
    [HideInInspector]
    public static GameManager instance = null;
	Dictionary<NetworkInstanceId, int> playerScore = new Dictionary<NetworkInstanceId, int>();
    public int scoreAdded = 10;
	private float timer;
    public float addScoreTimer = 1f;
	public float pointAdder;
    /// <summary>
    /// It is for instantiating one only player starting from main scene.
    /// </summary>
    public Transform spawnPoint;
    /// <summary>
    /// It is for instantiating one only player starting from main scene.
    /// </summary>
    public GameObject playerPrefab;

    /// <summary>
    /// It is for instantiating one only player starting from main scene.
    /// </summary>
    public CameraTargetPosition mainCamera;
	private SceneController sceneController;
	//for debug
	[HideInInspector]
	public List<int> playerScores = new List<int>();


	[SyncVar]public float roundTimer;

    private void Awake()
    {
        Debug.Log("Cancellami");
        if(instance == null)
        {
            instance = this;
        }
        
    }

    void Start()
    {
		sceneController = new SceneController ();
        InstantiatePlayerOffline();
    }
	
    /*private void getpise<T>(T param) {
        Debug.Log(param);
    }*/

	void Update () {
		NetworkInstanceId netId;
        //TO DO change getinstanceID to netID
        //getpise<string>("ciao");
        timer += Time.deltaTime;
       foreach(GameObject player in playerList)
        {
			netId = player.GetComponent<NetworkIdentity> ().netId;
			if (playerOnControlPoint.Contains(netId))
            {
				//Debug.Log ("Sono nel control point netId " + netId);
				if(timer>pointAdder){
					//Debug.Log ("Mi sto aggiungendo i punti");
					RpcAddScore(netId,scoreAdded);
					timer = 0;
				} 
            }
        }

		TimerUpdate ();
		//for debug
		foreach(NetworkInstanceId key in playerScore.Keys){
			int val = playerScore [key];
			Debug.Log ("Punteggio " + key + ": " + val);
			LevelUIManager.instance.UpdateMyScore (val);
		}

		LevelUIManager.instance.UpdateBestScores (playerScore);
	}
	[ClientRpc]
	void RpcAddScore(NetworkInstanceId playerID,int score)
    {
        if (playerScore.ContainsKey(playerID))
        {
            playerScore[playerID] += score;
        }
        else {
            playerScore.Add(playerID, score);
        }
    }

	public void PlayersOnPayload(Collider other,bool add){
		NetworkIdentity identity;

		if(isServer){
			identity = other.gameObject.GetComponent<NetworkIdentity> ();
			if (identity != null) {
				if (add)
					GameManager.instance.playerOnControlPoint.Add (identity.netId);
				else
					GameManager.instance.playerOnControlPoint.Remove (identity.netId);
			}
		}

	}

    public void InstantiatePlayerOffline()
    {
        NetworkLobbyManager lobbyManager = FindObjectOfType<NetworkLobbyManager>();
        if (lobbyManager == null)
        {
            GameObject player = Instantiate(playerPrefab, spawnPoint.position, playerPrefab.transform.rotation) as GameObject;
            PlayerControllerInGame playerController = player.GetComponent<PlayerControllerInGame>();
            Hook playerHook = player.GetComponent<Hook>();
            Rigidbody playerRb = player.GetComponent<Rigidbody>();
            Transform face = player.transform.FindChild("Face");
            playerController.myRb = playerRb;
            playerController.faceTransform = face;
			playerController.mouseLook = face.GetComponent<MouseLook>();
            playerController.isOffline = true;
            playerController.playerStatus = PlayerControllerInGame.PlayerStates.ground;
            playerController.distToGround = player.GetComponent<Collider>().bounds.extents.y;
            playerHook.firstPersonCamera = mainCamera.transform;
            playerHook.playerRigidbody = playerRb;
            playerHook.playerController = playerController;
            mainCamera.cameraPosTarget = face;
            mainCamera.gameObject.SetActive(true);
        }
    }

	private void TimerUpdate ()
	{
		roundTimer = Mathf.Clamp (roundTimer - Time.deltaTime, 0, float.MaxValue);
		LevelUIManager.instance.SetTimer (roundTimer);
		if (roundTimer == 0) {
			sceneController.QuitGame ();
		}
	}

	public void AddPlayerToList(GameObject player){
		Debug.Log ("add player");
		playerList.Add (player);
	}


}
