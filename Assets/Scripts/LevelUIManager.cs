﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelUIManager : NetworkBehaviour {
	public static LevelUIManager instance;
	public UISprite iconSprite;
	public UILabel timer;	
	public UILabel myScore;
	public UILabel bestScore;
	public GameObject gm;
	void Awake(){
		instance = this;
	}
	void Start(){
		gm.SetActive (true);
	}

	void Update(){
		SetWeaponIcon ();
	}

	public void SetTimer(float timer){
		int minutes;
		int seconds;

		minutes = Mathf.FloorToInt (timer / 60f);
		seconds = Mathf.FloorToInt (timer - minutes * 60);
		this.timer.text = string.Format ("{0:0}:{1:00}", minutes, seconds);
	}

	public void SetBestPlayers(List<GameObject> playerList){
		foreach (GameObject player in playerList) {
			
		}
	}

	public void SetWeaponIcon(){
		Quaternion iconRotation = iconSprite.gameObject.transform.rotation;

		if (Input.GetMouseButton (0)) {
			iconSprite.flip = UIBasicSprite.Flip.Nothing;
		} else if (Input.GetMouseButton (1)) {
			iconSprite.flip = UIBasicSprite.Flip.Vertically;
		} else {
			iconSprite.flip = UIBasicSprite.Flip.Nothing;
		}

	}

	public void UpdateMyScore(int score){
		myScore.text = "My Score:" + score.ToString ();
	}

	public void UpdateBestScores(Dictionary<NetworkInstanceId,int> playerScore){
		foreach(NetworkInstanceId key in playerScore.Keys){
			int val = playerScore [key];
			bestScore.text = "Player " + key + ": " + val;
		}
	}

}
