﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private string lobbySceneName = "LobbyScene";

    public UIButton startButton;
    public UIButton quitButton;

    private SceneController sceneController = new SceneController();

    private EventDelegate eventDelegate;

    private void Start()
    {
        eventDelegate = new EventDelegate(this, "LoadScene");
        eventDelegate.parameters[0].value = lobbySceneName;
        EventDelegate.Set(startButton.onClick, eventDelegate);
        eventDelegate = new EventDelegate(this, "QuitGame");
        EventDelegate.Set(quitButton.onClick, eventDelegate);
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    //.Add(new EventDelegate(sceneController.LoadScene(lobbySceneName))
}
