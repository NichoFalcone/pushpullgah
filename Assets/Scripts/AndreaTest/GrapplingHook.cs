﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingHook : MonoBehaviour
{
    public Transform target;
    public Transform start;
    public GameObject hook;
    public GameObject rope;
    private SpringJoint springJoint;
    private Rigidbody rigidbody;
    private Camera camera;
    private RaycastHit hit
    {
        get
        {
            return hit;
        }
        set
        {
            hit = value;
        }
    }

    private void Awake()
    {
        springJoint = GetComponent<SpringJoint>();
        rigidbody = GetComponent<Rigidbody>();
        camera = GetComponentInChildren<Camera>();
    }

    private void Update()
    {
        AimTarget();
    }

    private void AimTarget()
    {
        Physics.Raycast(camera.transform.position, camera.transform.forward);
        Debug.DrawRay(camera.transform.position, camera.transform.forward);
    }

    private void EnableSpringJoint()
    {

    }

    private void ManageSpringJoint()
    {

    }

    private void DisableSpringJoint()
    {

    }

    private void CalculateAccelerationAngle()
    {

    }

    private void AccelerateRigidbody()
    {

    }
}
