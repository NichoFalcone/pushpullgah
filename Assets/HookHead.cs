﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookHead : MonoBehaviour {
    Rigidbody rb;
    float hookSpeed = 50;
    void Awake() {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward*hookSpeed;
    }
    void OnCollisionEnter(Collision other) {
        if (!other.gameObject.CompareTag("Player")) {

            rb.isKinematic = true;
        }
    }
}
